import com.ppt.model.NumVo;
import com.ppt.pptutil.PPTUtil;
import org.apache.poi.sl.usermodel.TableCell;
import org.apache.poi.sl.usermodel.TextParagraph;
import org.apache.poi.xslf.usermodel.*;

import java.awt.*;
import java.text.NumberFormat;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;

/**
 * @author Jack
 */
public class ExportTest {

	private static final List<NumVo> propertys = new ArrayList<>();
	private static final List<NumVo> incomes = new ArrayList<>();
	private static final List<NumVo> expands = new ArrayList<>();


	public static void main(String[] args) {
		PPTUtil pptUtil = new PPTUtil("C:\\Users\\11736\\Desktop\\template.pptx");
		renderTables(pptUtil);
		renderTexts(pptUtil);
		renderCharts(pptUtil);
		pptUtil.writePPT("C:\\Users\\11736\\Desktop\\output.pptx");
	}

	private static void renderTables(PPTUtil pptUtil) {
		renderTable5_1(pptUtil);//家庭成员信息表(4)
		renderTable6_1(pptUtil);//资产负债表(5)
		renderTable7_1(pptUtil);//收入支出表(7)
		renderTable8_1(pptUtil);//财务分析与诊断(8)
		renderTable12_1(pptUtil);//家庭风险保障现状(12)
		renderTable17_1(pptUtil);//配置建议[现阶段家庭重疾险保障需求](17)
		renderTable18_1(pptUtil);//配置建议[资产调整建议](18)
		renderTable18_2(pptUtil);//配置建议[年结余规划](18)
		renderTable19_20(pptUtil);//配置建议[年化收益](19-20)
		renderTable21_22(pptUtil);//配置建议[年化收益]
		renderTable24(pptUtil);//配置建议[资产配置后前后变化]
	}

	private static void renderTexts(PPTUtil pptUtil) {
		renderTextOnPage1(pptUtil);//首页(0)
		renderTextOnPage4(pptUtil);//致函(4)
		renderTextOnPage5(pptUtil);//家庭财务需求/家庭现状(5)
		renderTextOnPage9(pptUtil);//财务比率分析[流动性比率/负债比率](9)
		renderTextOnPage10(pptUtil);//财务比率分析[清偿比率/结余比率/投资比率](10)
		renderTextOnPage11(pptUtil);//财务比率分析[负债收入比率/被动收入比率/小结](11)
		renderTextOnPage12(pptUtil);//家庭风险保障分析[分析结果2](12)
		renderTextOnPage13(pptUtil);//家庭风险保障分析[分析结果3/分析结果4/小结](13)
		renderTextOnPage14(pptUtil);//资产结构及收支结构分析[分析结果1](14)
		renderTextOnPage15(pptUtil);//资产结构及收支结构分析[分析结果2/小结](15)
		renderTextOnPage16(pptUtil);//配置建议[建议1/建议2](16)
		renderTextOnPage17(pptUtil);//配置建议[建议3](17)
		renderTextOnPage18(pptUtil);//配置建议[建议4](18)
		renderTextOnPage19(pptUtil);//配置建议[表格描述3]
		renderTextOnPage20(pptUtil);//配置建议[表格描述4]
		renderTextOnPage21(pptUtil);//配置建议[表格描述5]
		renderTextOnPage22(pptUtil);//配置建议[表格描述6]
		renderTextOnPage25(pptUtil);//最后日期
	}

	private static void renderCharts(PPTUtil pptUtil) {
		renderChart6_1(pptUtil);//资产结构图(6)
		renderChart7_1(pptUtil);//收入结构图(7)
		renderChart7_2(pptUtil);//支出结构图(7)
		renderChart8_1(pptUtil);//资产配置布局(8)
		renderChart9_1(pptUtil);//流动性比率(9)
		renderChart9_2(pptUtil);//负债比率(9)
		renderChart10_1(pptUtil);//结余比率(10)
		renderChart10_2(pptUtil);//投资比率(10)
		renderChart11_1(pptUtil);//负债收入比率(11)
		renderChart11_2(pptUtil);//被动收入比率(11)
		renderChart24_1(pptUtil);//资产配置前后变化
	}

	private static void renderChart6_1(PPTUtil pptUtil) {
		List<NumVo> datas = new ArrayList<>();
		propertys.stream().collect(Collectors.groupingBy(NumVo::getCategory, Collectors.summingDouble(NumVo::getValue)))
				.forEach((k, v) -> datas.add(new NumVo(k, v)));
		XSLFChart chart = pptUtil.getChartFromSlide(pptUtil.getSlides().get(5));
		List<List<String>> data = new ArrayList<>();
		List<String> names = new ArrayList<>();
		List<String> values = new ArrayList<>();
		Collections.shuffle(datas);
		datas.forEach(x -> {
			names.add(x.getName());
			values.add(String.valueOf(x.getValue()));
		});
		data.add(names);
		data.add(values);
		pptUtil.updatePieCat(pptUtil.getPieChartFromChart(chart).get(0), 0, data);
		pptUtil.updatePieDataCache(pptUtil.getPieChartFromChart(chart).get(0), 0, values);
	}

	private static void renderChart7_1(PPTUtil pptUtil) {
		List<NumVo> datas = new ArrayList<>();
		incomes.stream().collect(Collectors.groupingBy(NumVo::getCategory, Collectors.summingDouble(NumVo::getValue)))
				.forEach((k, v) -> datas.add(new NumVo(k, v)));
		List<XSLFChart> charts = pptUtil.getAllChartFromSlide(pptUtil.getSlides().get(6));
		List<List<String>> data = new ArrayList<>();
		List<String> names = new ArrayList<>();
		List<String> values = new ArrayList<>();
		Collections.shuffle(datas);
		datas.forEach(x -> {
			names.add(x.getName());
			values.add(String.valueOf(x.getValue()));
		});
		data.add(names);
		data.add(values);
		pptUtil.updatePieCat(pptUtil.getPieChartFromChart(charts.get(0)).get(0), 0, data);
		pptUtil.updatePieDataCache(pptUtil.getPieChartFromChart(charts.get(0)).get(0), 0, values);
	}

	private static void renderChart7_2(PPTUtil pptUtil) {
		List<NumVo> datas = new ArrayList<>();
		expands.stream().collect(Collectors.groupingBy(NumVo::getName, Collectors.summingDouble(NumVo::getValue)))
				.forEach((k, v) -> datas.add(new NumVo(k, v)));
		List<XSLFChart> charts = pptUtil.getAllChartFromSlide(pptUtil.getSlides().get(6));
		List<List<String>> data = new ArrayList<>();
		List<String> names = new ArrayList<>();
		List<String> values = new ArrayList<>();
		Collections.shuffle(datas);
		datas.forEach(x -> {
			names.add(x.getName());
			values.add(String.valueOf(x.getValue()));
		});
		data.add(names);
		data.add(values);
		pptUtil.updatePieCat(pptUtil.getPieChartFromChart(charts.get(1)).get(0), 0, data);
		pptUtil.updatePieDataCache(pptUtil.getPieChartFromChart(charts.get(1)).get(0), 0, values);
	}

	private static void renderChart8_1(PPTUtil pptUtil) {
		List<List<String>> datas = new ArrayList<>();
		List<String> columns = new ArrayList<>();
		List<String> standardValues = new ArrayList<>();
		List<String> values = new ArrayList<>();
		columns.add("流动性");
		columns.add("防御性");
		columns.add("收益性");
		standardValues.add("100");
		standardValues.add("100");
		standardValues.add("100");
		values.add("110");
		values.add("130");
		values.add("190");
		datas.add(columns);
		datas.add(values);
		XSLFChart chart = pptUtil.getChartFromSlide(pptUtil.getSlides().get(7));
		pptUtil.updateRadarCat(pptUtil.getRadarChartFromChart(chart).get(0), 0, datas);
		pptUtil.updateRadarDataCache(pptUtil.getRadarChartFromChart(chart).get(0), 0, standardValues);
		pptUtil.updateRadarCat(pptUtil.getRadarChartFromChart(chart).get(0), 1, datas);
		pptUtil.updateRadarDataCache(pptUtil.getRadarChartFromChart(chart).get(0), 1, values);
	}

	private static void renderChart9_1(PPTUtil pptUtil) {
		List<NumVo> datas = Arrays.asList(new NumVo("流动性资金", 160.0), new NumVo("家庭月支出", 8.67));
		List<XSLFChart> charts = pptUtil.getAllChartFromSlide(pptUtil.getSlides().get(8));
		List<List<String>> data = new ArrayList<>();
		List<String> names = new ArrayList<>();
		List<String> values = new ArrayList<>();
		Collections.shuffle(datas);
		datas.forEach(x -> {
			names.add(x.getName());
			values.add(String.valueOf(x.getValue()));
		});
		data.add(names);
		data.add(values);
		pptUtil.updateBarCat(pptUtil.getBarChartFromChart(charts.get(0)).get(0), 0, data);
		pptUtil.updateBarDataCache(pptUtil.getBarChartFromChart(charts.get(0)).get(0), 0, values);
	}

	private static void renderChart9_2(PPTUtil pptUtil) {
		List<NumVo> datas = Arrays.asList(new NumVo("净资产总额", 95.0), new NumVo("负债总额", 5.0));
		List<XSLFChart> charts = pptUtil.getAllChartFromSlide(pptUtil.getSlides().get(8));
		List<List<String>> data = new ArrayList<>();
		List<String> names = new ArrayList<>();
		List<String> values = new ArrayList<>();
		Collections.shuffle(datas);
		datas.forEach(x -> {
			names.add(x.getName());
			values.add(String.valueOf(x.getValue()));
		});
		data.add(names);
		data.add(values);
		pptUtil.updateDoughnutChart(pptUtil.getDoughnutChart(charts.get(1)).get(0), 0, data);
		pptUtil.updateDoughnutDataChart(pptUtil.getDoughnutChart(charts.get(1)).get(0), 0, values);
	}

	private static void renderChart10_1(PPTUtil pptUtil) {
		List<NumVo> datas = Arrays.asList(new NumVo("年结余", 118.0), new NumVo("年支出", 123.0));
		List<XSLFChart> charts = pptUtil.getAllChartFromSlide(pptUtil.getSlides().get(9));
		List<List<String>> data = new ArrayList<>();
		List<String> names = new ArrayList<>();
		List<String> values = new ArrayList<>();
		Collections.shuffle(datas);
		datas.forEach(x -> {
			names.add(x.getName());
			values.add(String.valueOf(x.getValue()));
		});
		data.add(names);
		data.add(values);
		pptUtil.updateBarCat(pptUtil.getBarChartFromChart(charts.get(0)).get(0), 0, data);
		pptUtil.updateBarDataCache(pptUtil.getBarChartFromChart(charts.get(0)).get(0), 0, singletonList(values.get(0)));
		pptUtil.updateBarCat(pptUtil.getBarChartFromChart(charts.get(0)).get(0), 1, data);
		pptUtil.updateBarDataCache(pptUtil.getBarChartFromChart(charts.get(0)).get(0), 1,
				singletonList((values.get(1))));
	}

	private static void renderChart10_2(PPTUtil pptUtil) {
		List<NumVo> datas = Arrays.asList(new NumVo("非投资资产", 33.0), new NumVo("投资资产", 67.0));
		List<XSLFChart> charts = pptUtil.getAllChartFromSlide(pptUtil.getSlides().get(9));
		List<List<String>> data = new ArrayList<>();
		List<String> names = new ArrayList<>();
		List<String> values = new ArrayList<>();
		Collections.shuffle(datas);
		datas.forEach(x -> {
			names.add(x.getName());
			values.add(String.valueOf(x.getValue()));
		});
		data.add(names);
		data.add(values);
		pptUtil.updateDoughnutChart(pptUtil.getDoughnutChart(charts.get(1)).get(0), 0, data);
		pptUtil.updateDoughnutDataChart(pptUtil.getDoughnutChart(charts.get(1)).get(0), 0, values);
	}

	private static void renderChart11_1(PPTUtil pptUtil) {
		List<NumVo> datas = Arrays.asList(new NumVo("当年贷款支出", 10.0), new NumVo("其他支出+结余", 90.0));
		List<XSLFChart> charts = pptUtil.getAllChartFromSlide(pptUtil.getSlides().get(10));
		List<List<String>> data = new ArrayList<>();
		List<String> names = new ArrayList<>();
		List<String> values = new ArrayList<>();
		Collections.shuffle(datas);
		datas.forEach(x -> {
			names.add(x.getName());
			values.add(String.valueOf(x.getValue()));
		});
		data.add(names);
		data.add(values);
		pptUtil.updateDoughnutChart(pptUtil.getDoughnutChart(charts.get(0)).get(0), 0, data);
		pptUtil.updateDoughnutDataChart(pptUtil.getDoughnutChart(charts.get(0)).get(0), 0, values);
	}

	private static void renderChart11_2(PPTUtil pptUtil) {
		List<NumVo> datas = Arrays.asList(new NumVo("被动收入覆盖支出", 33.0), new NumVo("工作收入覆盖支出", 67.0));
		List<XSLFChart> charts = pptUtil.getAllChartFromSlide(pptUtil.getSlides().get(10));
		List<List<String>> data = new ArrayList<>();
		List<String> names = new ArrayList<>();
		List<String> values = new ArrayList<>();
		Collections.shuffle(datas);
		datas.forEach(x -> {
			names.add(x.getName());
			values.add(String.valueOf(x.getValue()));
		});
		data.add(names);
		data.add(values);
		pptUtil.updateDoughnutChart(pptUtil.getDoughnutChart(charts.get(1)).get(0), 0, data);
		pptUtil.updateDoughnutDataChart(pptUtil.getDoughnutChart(charts.get(1)).get(0), 0, values);
	}

	private static void renderChart24_1(PPTUtil pptUtil) {
		List<XSLFChart> charts = pptUtil.getAllChartFromSlide(pptUtil.getSlides().get(23));
		List<List<String>> data = new ArrayList<>();
		Map<String, List<Double>> values = new LinkedHashMap<>();
		values.put("工作性收入", Arrays.asList(200.0, 200.0, 200.0));
		values.put("固定资产租金收入", Arrays.asList(9.5, 9.5, 9.5));
		values.put("企业分红收入", Arrays.asList(2.0, 2.0, 2.0));
		values.put("金融资产投资收益", Arrays.asList(24.0, 72.0, 323.0));
		List<String> names = Arrays.asList("配置前", "【第5年】配置后", "【第10年】配置后");
		data.add(names);
		int i = 0;
		data.add(new ArrayList<>(values.keySet()));
		for (List<Double> vals : values.values()) {
			pptUtil.updateBarCat(pptUtil.getBarChartFromChart(charts.get(0)).get(0), i, data);
			pptUtil.updateBarDataCache(pptUtil.getBarChartFromChart(charts.get(0)).get(0), i,
					vals.stream().map(String::valueOf).collect(Collectors.toList()));
			i++;
		}
	}


	private static void renderTable5_1(PPTUtil pptUtil) {
		List<XSLFTable> tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(4));
		List<List<String>> values = new ArrayList<>();
		values.add(Arrays.asList("本人", "1982.9.5", "40", "男", "2042年"));
		values.add(Arrays.asList("配偶", "1983.3.10", "39", "女", "2042年"));
		appendRow(pptUtil, tables.get(0), 10, 15, values);
	}

	private static void renderTable6_1(PPTUtil pptUtil) {
		List<List<String>> values;
		List<XSLFTable> tables;
		tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(5));
		List<NumVo> debts = new ArrayList<>();
		propertys.add(new NumVo("流动资金", "活期存款", 100.0));
		propertys.add(new NumVo("流动资金", "银行理财（≤3个月）", 50.0));
		propertys.add(new NumVo("金融资产", "股票", 250.0));
		propertys.add(new NumVo("金融资产", "公募基金", 150.0));
		propertys.add(new NumVo("金融资产", "私募基金", 400.0));
		propertys.add(new NumVo("固定资产（自住）", "自住房", 800.0));
		propertys.add(new NumVo("固定资产（自住）", "自住车位", 30.0));
		propertys.add(new NumVo("固定资产（投资）", "投资住宅1", 300.0));
		propertys.add(new NumVo("固定资产（投资）", "投资公寓1", 200.0));
		propertys.add(new NumVo("固定资产（投资）", "投资公寓2", 200.5));
		propertys.add(new NumVo("固定资产（投资）", "投资商铺", 1502.0));
		propertys.add(new NumVo("公司股权", "公司股权（非参与经营）", 50.0));
		propertys.add(new NumVo("其他资产", "机动车", 50.0));
		debts.add(new NumVo("投资住宅房贷", 70.0));
		debts.add(new NumVo("投资公寓房贷", 80.0));
		debts.add(new NumVo("车贷", 20.0));
		values = new ArrayList<>();
		String lastCategory = propertys.get(0).getCategory();
		List<Integer> rowSpans = new ArrayList<>();
		for (int i = 0; i < propertys.size(); i++) {
			NumVo property = propertys.get(i);
			NumVo debt;
			if (i < debts.size()) {
				debt = debts.get(i);
			} else {
				debt = new NumVo("", null);
			}
			values.add(
					Arrays.asList(property.getCategory(), property.getName(), fmt(property.getValue()), debt.getName(),
							debt.getValue() == null ? "" : fmt(debt.getValue())));
			if (!Objects.equals(lastCategory, property.getCategory())) {
				rowSpans.add(i);
			}
			lastCategory = property.getCategory();
		}
		final XSLFTable table = tables.get(0);
		appendRow(pptUtil, table, 10, 2, values);
		int lastIndex = 1;
		for (int lastRows : rowSpans) {
			if (lastRows - lastIndex > 0) {
				table.mergeCells(lastIndex, lastRows, 0, 0);
				pptUtil.getTableRows(table).get(lastIndex).getCells().get(0).setTopInset((10 * 2));
			}
			lastIndex = lastRows + 1;
		}
		double totaPpropertys = propertys.stream().mapToDouble(NumVo::getValue).sum(), totlalDebts = debts.stream()
				.mapToDouble(NumVo::getValue).sum();
		XSLFTableRow row = table.addRow();
		createBoldCell(pptUtil, row.addCell(), 12, "资产总计", 2);
		createBoldCell(pptUtil, row.addCell(), 12, "", 2);
		createBoldCell(pptUtil, row.addCell(), 12, fmt(totaPpropertys), 2);
		createBoldCell(pptUtil, row.addCell(), 12, "负债合计", 2);
		createBoldCell(pptUtil, row.addCell(), 12, fmt(totlalDebts), 2);
		row.mergeCells(0, 1);
		row = table.addRow();
		createBoldCell(pptUtil, row.addCell(), 12, "净资产", 2);
		createBoldCell(pptUtil, row.addCell(), 12, "", 2);
		createBoldCell(pptUtil, row.addCell(), 12, fmt(totaPpropertys - totlalDebts), 2);
		createBoldCell(pptUtil, row.addCell(), 12, "", 2);
		createBoldCell(pptUtil, row.addCell(), 12, "", 2);
		row.mergeCells(0, 1);
		row.mergeCells(2, 4);
		row.getCells().forEach(c -> pptUtil.setCellFillColor(c, "#CFB478"));
	}

	private static void renderTable7_1(PPTUtil pptUtil) {
		List<XSLFTable> tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(6));
		incomes.add(new NumVo("工作性收入", "本人收入", 150.0));
		incomes.add(new NumVo("工作性收入", "配偶收入", 50.0));
		incomes.add(new NumVo("固定资产\r租金收入", "投资住宅租金", 3.0));
		incomes.add(new NumVo("固定资产\r租金收入", "投资公寓租金", 3.5));
		incomes.add(new NumVo("固定资产\r租金收入", "投资商铺金", 3.0));
		incomes.add(new NumVo("金融资产投资收益", "股票", 7.0));
		incomes.add(new NumVo("金融资产投资收益", "公募基金", 2.0));
		incomes.add(new NumVo("金融资产投资收益", "私募基金", 15.0));
		incomes.add(new NumVo("企业分红收入", "企业分红收入（非参与经营）", 2.0));

		expands.add(new NumVo("日常生活支出", 36.0));
		expands.add(new NumVo("子女教育", 20.0));
		expands.add(new NumVo("赡养父母", 5.0));
		expands.add(new NumVo("休闲娱乐", 5.0));
		expands.add(new NumVo("旅游支出", 10.0));
		expands.add(new NumVo("保险支出", 20.0));
		expands.add(new NumVo("住房还贷", 15.0));
		expands.add(new NumVo("车贷还贷", 5.0));

		List<List<String>> values = new ArrayList<>();
		String lastCategory = incomes.get(0).getCategory();
		List<Integer> rowSpans = new ArrayList<>();
		for (int i = 0; i < incomes.size(); i++) {
			NumVo property = incomes.get(i);
			NumVo debt;
			if (i < expands.size()) {
				debt = expands.get(i);
			} else {
				debt = new NumVo("", null);
			}
			values.add(
					Arrays.asList(property.getCategory(), property.getName(), fmt(property.getValue()), debt.getName(),
							debt.getValue() == null ? "" : fmt(debt.getValue())));
			if (!Objects.equals(lastCategory, property.getCategory())) {
				rowSpans.add(i);
			}
			lastCategory = property.getCategory();
		}
		final XSLFTable table = tables.get(0);
		appendRow(pptUtil, table, 10, 2, values);
		int lastIndex = 1;
		for (int lastRows : rowSpans) {
			if (lastRows - lastIndex > 0) {
				table.mergeCells(lastIndex, lastRows, 0, 0);
				pptUtil.getTableRows(table).get(lastIndex).getCells().get(0).setTopInset((10 * 2));
			}
			lastIndex = lastRows + 1;
		}
		double totalIncome = incomes.stream().mapToDouble(NumVo::getValue).sum(), totalExpand = expands.stream()
				.mapToDouble(NumVo::getValue).sum();
		XSLFTableRow row = table.addRow();
		createBoldCell(pptUtil, row.addCell(), 12, "收入合计", 2);
		createBoldCell(pptUtil, row.addCell(), 12, "", 2);
		createBoldCell(pptUtil, row.addCell(), 12, fmt(totalIncome), 2);
		createBoldCell(pptUtil, row.addCell(), 12, "支出合计", 2);
		createBoldCell(pptUtil, row.addCell(), 12, fmt(totalExpand), 2);
		row.mergeCells(0, 1);
		row = table.addRow();
		createBoldCell(pptUtil, row.addCell(), 12, "年度结余", 2);
		createBoldCell(pptUtil, row.addCell(), 12, "", 2);
		createBoldCell(pptUtil, row.addCell(), 12, fmt(totalIncome - totalExpand), 2);
		createBoldCell(pptUtil, row.addCell(), 12, "", 2);
		createBoldCell(pptUtil, row.addCell(), 12, "", 2);
		row.mergeCells(0, 1);
		row.mergeCells(2, 4);
		row.getCells().forEach(c -> pptUtil.setCellFillColor(c, "#CFB478"));
	}

	private static void renderTable8_1(PPTUtil pptUtil) {
		List<XSLFTable> tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(7));
		List<Double> values = Arrays.asList(15.52, 6.23, 93.77, 51.16, 66.41, 8.42, 30.60);
		XSLFTable table = tables.get(0);
		List<XSLFTableRow> rows = table.getRows();
		for (int i = 1; i < rows.size(); i++) {
			if (i - 1 < values.size()) {
				XSLFTableCell cell = rows.get(i).getCells().get(3);
				XSLFTextParagraph textParagraph = pptUtil.addCellParagraph(cell);
				pptUtil.addParagraphText(textParagraph, true, values.get(i - 1) + "%");
				pptUtil.setCellHorizontalAlign(cell, "center");
				pptUtil.setCellVerticalAlign(cell, "center");
				cell.setTopInset(5);
				cell.setBottomInset(15);
			}
		}
	}

	private static void renderTable12_1(PPTUtil pptUtil) {
		List<XSLFTable> tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(11));
		List<List<String>> values = new ArrayList<>();
		//家庭成员/是否有社保/人寿险(保额)/人寿险(保费)/重疾险(保额)/重疾险(保费)/年金险(保额)/年金险(保费)/医疗险(保额)/医疗险(保费)/意外险(保额)/意外险(保费)/合计年交保费（元）
		values.add(Arrays.asList("本人", "是", "20", "15000", "15", "15000", "-", "100", "350", "10", "100", "24450"));
		values.add(Arrays.asList("配偶", "是", "20", "15000", "15", "15000", "-", "100", "350", "10", "100", "24450"));
		values.add(Arrays.asList("儿子", "是", "20", "15000", "15", "15000", "-", "100", "350", "10", "100", "24450"));
		values.add(Arrays.asList("女儿", "是", "20", "15000", "15", "15000", "-", "100", "350", "10", "100", "24450"));
		appendRow(pptUtil, tables.get(0), 8, 5, values);
	}

	private static void renderTable17_1(PPTUtil pptUtil) {
		List<XSLFTable> tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(16));
		List<List<String>> values = new ArrayList<>();
		values.add(Arrays.asList("本人", "15", "50-100", "35-85"));
		values.add(Arrays.asList("配偶", "10", "50-100", "35-85"));
		appendRow(pptUtil, tables.get(0), 10, 5, values);
	}

	private static void renderTable18_1(PPTUtil pptUtil) {
		List<XSLFTable> tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(17));
		List<List<String>> values = new ArrayList<>();
		values.add(Arrays.asList("1", "银行存款", "100", "60", "40", "保留60万足够用于流动资金储备"));
		values.add(Arrays.asList("2", "银行理财", "50", "0", "50", "流动资金储备已充足"));
		appendRow(pptUtil, tables.get(0), 10, 2, values);
		XSLFTableRow row = tables.get(0).addRow();
		createCell(pptUtil, row.addCell(), 10, "合计", 2);
		createCell(pptUtil, row.addCell(), 10, "", 2);
		createCell(pptUtil, row.addCell(), 10, "", 2);
		createCell(pptUtil, row.addCell(), 10, "", 2);
		createCell(pptUtil, row.addCell(), 10, "390", 2);
		createCell(pptUtil, row.addCell(), 10, "", 2);
		row.mergeCells(0, 3);
	}

	private static void renderTable18_2(PPTUtil pptUtil) {
		List<XSLFTable> tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(17));
		List<List<String>> values = new ArrayList<>();
		values.add(Arrays.asList("121.5", "6.5", "115"));
		appendRow(pptUtil, tables.get(1), 10, 2, values);
	}

	private static void renderTable19_20(PPTUtil pptUtil) {
		List<XSLFTable> tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(18));
		List<List<String>> values = new ArrayList<>();
		for (int i = 0; i < 61; i++) {
			values.add(Arrays.asList(i + 1 + "", 2022 + i + "", 40 + i + "", "3,900,000.00", "", "3,900,000.00", "",
					"3,900,000.00"));
		}
		appendRow(pptUtil, tables.get(0), 8, 0, values.subList(0, Math.min(values.size(), 30)));
		if (values.size() > 30) {
			tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(19));
			appendRow(pptUtil, tables.get(0), 8, 0, values.subList(30, values.size()));
		}
	}

	private static void renderTable21_22(PPTUtil pptUtil) {
		List<XSLFTable> tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(20));
		List<List<String>> values = new ArrayList<>();
		for (int i = 0; i < 61; i++) {
			values.add(Arrays.asList(i + 1 + "", 2022 + i + "", 40 + i + "", "3,900,000.00", "", "3,900,000.00", "",
					"3,900,000.00"));
		}
		appendRow(pptUtil, tables.get(0), 8, 0, values.subList(0, Math.min(values.size(), 30)));
		if (values.size() > 30) {
			tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(21));
			appendRow(pptUtil, tables.get(0), 8, 0, values.subList(30, values.size()));
		}
	}

	private static void renderTable24(PPTUtil pptUtil) {
		List<XSLFTable> tables = pptUtil.getAllTableFromSlide(pptUtil.getSlides().get(23));
		Map<String, List<Double>> values = new LinkedHashMap<>();
		values.put("工作性收入", Arrays.asList(200.0, 200.0, 200.0));
		values.put("固定资产租金收入", Arrays.asList(9.5, 9.5, 9.5));
		values.put("金融资产投资收益", Arrays.asList(24.0, 72.0, 323.0));
		values.put("企业分红收入", Arrays.asList(2.0, 2.0, 2.0));
		appendRow(pptUtil, tables.get(0), 10, 10, values.entrySet().stream().map(x -> {
			List<String> vals = new ArrayList<>();
			vals.add(x.getKey());
			vals.addAll(x.getValue().stream().map(String::valueOf).collect(Collectors.toList()));
			return vals;
		}).collect(Collectors.toList()));

	}


	private static void renderTextOnPage5(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(4));
		List<String> requirements = new ArrayList<>();
		requirements.add("厘清家庭资产负债现状");
		requirements.add("子女教育规划");
		requirements.add("养老规划");
		requirements.add("构建家庭保障体系");
		requirements.add("希望资产每年实现8%-10%的增值");
		StringBuilder sql = new StringBuilder();
		for (int i = 0; i < requirements.size(); i++) {
			sql.append(i + 1).append("、").append(requirements.get(i));
			if (i < requirements.size() - 1) {
				sql.append("；");
			} else {
				sql.append("。");
			}
			sql.append("\r");
		}
		pptUtil.replaceTagInParagraph(textParagraphs.get(2), newMaps("requirements", sql.toString()));
	}

	private static void renderTextOnPage9(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(8));
		pptUtil.replaceTagInParagraph(textParagraphs.get(5), newMaps("liquidityRatio",
				"流动性比率反映流动资金的充裕程度，您的家庭年支出为116万，每月支出为9.67万，"
						+ "支出项主要集中在日常生活支出、子女教育、保险支出、住房还贷支出等"
						+ "，此类支出为家庭目前主要支出， 目前家庭流动资金为150万，相当于 15.52个月的家庭支出，一般情况,  有相对稳定收入来源的家庭，保留3-6个月的家庭总支出（约29万-58万），如收入不稳定的家庭，保留6-12个月的家庭总支出（约58万-116万），目前您家庭的流动性比率过高。\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(12),
				newMaps("debtRatio", "负债率比率反映的是家庭综合偿债能力，目前您家庭的负债率为6.23%，处在安全范围内，家庭偿债能力较强。\n"));
	}

	private static void renderTextOnPage10(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(9));
		pptUtil.replaceTagInParagraph(textParagraphs.get(2), newMaps("debtServiceRatio",
				"清偿比率反映的是家庭综合偿债能力，目前您家庭的清偿比率为93.77%，清偿比率高，反映家庭没有合理应用应债能力提高个人资产规模，需要进一步优化。\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(4),
				newMaps("balanceRatio", "结余比率反映的是家庭综合偿债能力，目前您家庭的结余比率为51.16%，家庭净资产增长能力强。\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(13),
				newMaps("investmentRatio", "投资比率反映的是家庭通过投资实现财富增长的能力，目前您家庭的结投资比率为66.41%，投资占比过高，投资收益预期增加的同时投资风险也在增大。\n"));
	}

	private static void renderTextOnPage11(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(10));
		pptUtil.replaceTagInParagraph(textParagraphs.get(2),
				newMaps("debtToIncomeRatio", "负债收入比率反映的是家庭当下的债务负荷程度，目前您家庭的负债收入比率为8.42%，家庭能够应付当前的债务。\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(11),
				newMaps("passiveIncomeRatio", "被动收入比率反映的家庭财务自由度，目前您家庭的被动收入比率为30.6%，家庭收入结构中工作性收入占重要部分，离家庭财务自由还有距离。\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(20), newMaps("summaryOfAnalysis",
				"财务比率用于分析家庭财务状况，七大比率的结果代表目前家庭财务整体健康状况，同时让我们清晰家庭财务是否处于合理状态。我们根据健康的财务比率，在未来家庭财务管理中不断优化家庭资产配置，让家庭财务比率持续趋于健康、合理的状态。\n"));
	}

	private static void renderTextOnPage12(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(11));
		pptUtil.replaceTagInParagraph(textParagraphs.get(18), newMaps("riskAnalysisTitle_2", "2、家庭风险保障缺口\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(19), newMaps("riskAnalysisContent_2",
				"您和您配偶作为家庭主要经济支柱，家人的高品质生活及孩子未来教育支出、养老生活主要依赖您的家庭收入。基于此，现阶段拥有足额的保障对家庭而言十分重要。\n"
						+ "一般情况下，家庭应配备的寿险保额为能覆盖10年日常生活支出+子女教育总费用+一年以上未偿还的贷款，根据您所提供的财务信息，当前家庭需要：36万*10+320万+170万=850万的保障来保证家人的生活品质，目前家庭总保障为20万，寿险保障缺口为850万-20万=830万。\n"
						+ "\n"
						+ "一般情况下，每位家庭成员的重疾保障应为50-100万。目前本人的重疾保额为15万，保障缺口为35万～85万；目前您配偶重疾保额为10万，保障缺口为40万～90万；您儿子的重疾保额为20万，保障缺口为30万～80万；您女儿的重疾保额为20万，保障缺口为30万～80万。\n"
						+ "\n" + "除了寿险及重疾保障外，医疗与意外的保障也均为家庭保障的必备之选！\n"));
	}

	private static void renderTextOnPage13(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(12));
		pptUtil.replaceTagInParagraph(textParagraphs.get(1), newMaps("riskAnalysisTitle_3", "3、家庭成员的保障配比欠合理\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(2), newMaps("riskAnalysisContent_3",
				"根据您提供的信息，目前家庭的保费支出为20万/年，其中子女的保费支出占比为75%，尤其是子女的年金保费支出为14万，占比家庭年保费支出的70%，家庭成员间的保障配比欠合理。\n" + "\n"
						+ "通常情况下，家庭的保障需求配置应遵循先以下原则：\n" + "（1）先大人后小孩，先保障险种后储蓄险种\n" + "（2）收入高的家庭成员应优先保障，\n"
						+ "（3）险种的配置顺序应为意外险、医疗险、重疾险、人寿险、年金险\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(3), newMaps("riskAnalysisTitle_4", "4、保费支出合理\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(4),
				newMaps("riskAnalysisContent_4", "通常情况下保费支出占家庭收入的10%左右。目前您的家庭保费支出为20万元，占比合理，可根据家庭实际需求适当增加保障。\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(5), newMaps("riskAnalysisTitle_5", " "));
		pptUtil.replaceTagInParagraph(textParagraphs.get(6), newMaps("riskAnalysisContent_5", " "));
		pptUtil.replaceTagInParagraph(textParagraphs.get(8), newMaps("summaryOfRiskAnalysis",
				"保险是家庭财富管理的必选配置，能为家人带来高品质的医疗保障，抵御家庭意外风险，提高家庭抗风险能力。保险还是家庭财富是最理想的保护工具，能规避多种风险，同时在家庭财富传承规划中，是唯一拥有杠杆功能的工具。所以合理有效的保险配置方案能够有效的满足家庭的风险管理、财富安全及财富传承的综合需求\n"));
	}

	private static void renderTextOnPage14(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(13));
		pptUtil.replaceTagInParagraph(textParagraphs.get(2), newMaps("structureAnalysisTitle_1", "1、资产结构\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(3), newMaps("structureAnalysisContent_1", "1）家庭资产流动性欠合理\n"
				+ "在整体家庭资产中，固定资产为1730万，占总资产的63.37%，占比相对较高。住宅、公寓、车位、商铺等固定资产变现能力弱，导致资产的流动性不足，万一发生风险,固定资产变现可能会面临资产折价，从目前的资产结构来讲，存在一定的流动性风险。\n"
				+ "（2）\uF0D8投资类固定资产未能带来合理的租金收入\n" + "在整体家庭资产中，投资类固定资产带来的租金收入过低，限制了家庭整体资产的增值能力，须对此项资产做出合理调整与规划。\n"
				+ "长期看国内房产已逐步回归保值功能，房产在家庭资产中占比将逐步降低，未来投资性固定资产的持有成本将增加，且变现能力减弱。未来除了核心城市核心地段房产或资源性房产有一定的配置价值，其它地区要慎重考虑。对于家庭资产结构来讲是否还要配置房产主要考虑的因素是：\n"
				+ "\n"
				+ "1、是否具有良好的流动性；2、是否能保持长期合理稳定的租售比；3、房产配置在家庭资产中占比是否合理。投资性房产对于您的家庭来说是一笔不小的支出，如果不作大调整，也应该优化该类资产，提升资产质量，或根据家庭未来需求，将现有家庭资产进行合理的资产配置，以提高现有资产的保值、增值能力。\n"
				+ "\n" + "（3）金融资产配置欠合理\n"
				+ "金融资产是未来家庭资产配置的重要方向，就您目前的家庭金融资产占比总投资资产47%，主要分布股票、公募基金、私募基金，近一年此类资产的收入为24万，占家庭投资收入10.19%"));
	}

	private static void renderTextOnPage15(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(14));
		pptUtil.replaceTagInParagraph(textParagraphs.get(1), newMaps("structureAnalysisTitle_2", "2、收入与支出结构\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(2), newMaps("structureAnalysisContent_2", "（1）收入结构欠合理\n"
				+ "当前您的家庭年收入为235.5万元，其中工作收入为200万元，占比84.93%，被动收入为35.5万元，占比15.07%。工作收入占比过高，家庭的收入主要依赖工作收入，意味着家庭高品质生活支出、孩子教育支出及未来的退休养老生活，高度依赖于“人赚钱”，抵御风险能力较弱。\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(4), newMaps("summaryOfStructureAnalysis",
				"家庭资产结构与收入支出结构是家庭财富管理的核心，资产结构是否合理直接影响了家庭资产的安全性，同时也体现了家庭资产配置的合理性、家庭资产保值增值的效率。而收入支出结构的合理性体现了家庭生活保障及抵抗风险的能力，我们要时刻关注家庭的资产结构与收入支出结构，以保证家庭财富与需求处于健康合理状态。\n"));
	}

	private static void renderTextOnPage16(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(15));
		pptUtil.replaceTagInParagraph(textParagraphs.get(2), newMaps("suggestionsTitle_1", "1、流动资金"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(3), newMaps("suggestionsContent_1",
				"流动性比率反映流动资金的充裕程度，您的家庭年支出为116万，每月支出为9.67万，支出项主要集中在日常生活支出、子女教育、保险支出、住房还贷支出等，此类支出为家庭目前主要支出， 目前家庭流动资金为150万，相当于 15.52个月的储备资金，一般情况,  建议保留3～6个月的流动资金即可，约29万-58万，目前的流动性资金储备过高，过多的资金放在银行或流动性较强的货币基金收益都极低，意味着超额准备的流动资金难以增值，甚至在通胀的蚕食下贬值。建议可把部分闲置资金做进一步的优化选择。\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(4), newMaps("suggestionsTitle_2", "2、家庭风险管理建议"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(5), newMaps("suggestionsContent_2",
				"您和您配偶作为家庭的主要经济支柱，家人高品质的生活及孩子未来的教育支出及养老生活主要依赖您的家庭收入，基于此，现阶段拥有足额的保障对家庭而言十分重要。\n"
						+ "通过前面的诊断分析出当年您家庭的寿险缺口为830万，从家庭工作收入情况来看，您的收入占比为75%，建议您补充的寿险额度为830万*75%=623万；您配偶的收入占比为25%，建议您配偶补充的寿险额度为830万*25%=207万，以满足家庭的寿险风险保障。\n"
						+ "除了寿险外，充足的重疾险保障对家庭来说也尤为重要。通常情况下，我们建议每位家庭成员的重疾保障应配置为50-100万。通过前面的分析诊断，当前您的重疾保额为15万，目前您的配偶重疾保额为10万，儿子的重疾保额为20万，女儿的重疾保额为20万，为了满足家庭的重疾保障额度，我们建议家庭补充的重疾险额度如下："));
	}

	private static void renderTextOnPage17(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(16));
		pptUtil.replaceTagInParagraph(textParagraphs.get(8),
				newMaps("suggestionsTitle_3", "3、调整资产配置，构建安全合理的资产结构，满足家庭资产的保值增值需求，同时提高理财性收入的稳定性及确定性"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(9), newMaps("suggestionsContent_3", "（1）投资类固定资产建议"
				+ "家庭总资产中，投资性固定资产占总投资资产50%，占比相对合理，但未能给家庭带来合理的租金收入，建议在合适的机会优化此类资产，配置在核心地段的优质固定资产，以提升租金收益率，或盘活资产配置于效率更高的其他大类资产。\n"
				+ "（2）金融资产建议\n" + "家庭总资产中，金融资产占总投资资产47.06%，在过去2-3年未能给家庭带来理想性的理财收入，收入确定性较弱，建议针对现有的金融资产进行调整与优化。\n"
				+ "家庭财富管理的核心是资产配置，正确的大类资产配置能给家庭带来安全、持续合理的收益以满足家庭的财务需求。但合理有效的资产配置需要拥有专业能力：\n" + "（1）了解家庭风险耐受度的能力；\n"
				+ "（2）对宏观经济周期和大类资产轮动的判断能力；\n" + "（3）对宏观到中观的择时能力；\n" + "（4）对各类核心交易策略的理解能力；\n" + "（5）对资产配置的再平衡能力等。\n"
				+ "因此运用好金融工具做好家庭的金融资产配置满足未来家庭需求，达成财务目标需要依靠专业的顾问团队。根据家庭的现状与未来需求共同制定有效的配置方案，并进行不间断的动态管理。\n"
				+ "就目前家庭的财务情况及未来财务需求，我们将为您的家庭提供专属的配置方案，详见《投资规划》建议"));
	}

	private static void renderTextOnPage18(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(17));
		pptUtil.replaceTagInParagraph(textParagraphs.get(1), newMaps("suggestionsTitle_4", "4、投资建议\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(2),
				newMaps("suggestionsTableDesc_1", "根据上述的诊断分析，我们建议您将家庭现有的资产进行部分调整，具体调整如下：\n"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(16), newMaps("suggestionsTableDesc_2",
				"目前您家庭的年结余为121.5万，且该年结余较为稳定，建议保留6.5万用于家庭应急支出，剩余的115万用于投资，每年定期投入，直至退休。\n"));
	}

	private static void renderTextOnPage19(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(18));
		pptUtil.replaceTagInParagraph(textParagraphs.get(1), newMaps("suggestionsTableDesc_3",
				"根据您家庭的财务需求，目前本人年龄为40岁，配偶为39岁，预计1年后（即2023年）需要支付子女的教育费用40万/年，共8年，夫妻俩计划2042年开始退休，退休后的理财生活费用为30万/年，我们将调整出来的资金及家庭年结余做以下投资规划，投资年化收益率为6%-8%，以满足家庭的财务需求。\n"
						+ "预期投资组合收益率为6%：\n"));
	}

	private static void renderTextOnPage20(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(19));
		pptUtil.replaceTagInParagraph(textParagraphs.get(5), newMaps("suggestionsTableDesc_4",
				"通过上述表格可以看到，利用6%的投资组合年化收益率演算，在2023年至2030年满足家庭8年两个的子女教育支出及夫妻60岁后每年30万（现值）的养老生活，在您100岁时，投资组合账户剩余28,729万用作传承。\n"));
	}

	private static void renderTextOnPage21(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(20));
		pptUtil.replaceTagInParagraph(textParagraphs.get(2), newMaps("suggestionsTableDesc_5", "预期投资组合收益率为8%：\n"));
	}

	private static void renderTextOnPage22(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(21));
		pptUtil.replaceTagInParagraph(textParagraphs.get(2), newMaps("suggestionsTableDesc_6",
				"通过上述表格可以看到，利用8%的投资组合年化收益率演算，在2023年至2030年满足家庭8年两个的子女教育支出及夫妻60岁后每年30万（现值）的养老生活，在您100岁时，投资组合账户剩余104,737万用作传承。\n"));
	}

	private static void renderTextOnPage25(PPTUtil pptUtil){
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(24));
		pptUtil.replaceTagInParagraph(textParagraphs.get(11), newMaps("times", "2022年12月"));
	}


	private static void renderTextOnPage4(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(3));
		pptUtil.replaceTagInParagraph(textParagraphs.get(0), newMaps("name", "张先生"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(7), newMaps("times", "2022年12月"));
	}

	private static void renderTextOnPage1(PPTUtil pptUtil) {
		List<XSLFTextParagraph> textParagraphs = pptUtil.getParagraphsFromSlide(pptUtil.getSlides().get(0));
		pptUtil.replaceTagInParagraph(textParagraphs.get(0), newMaps("name", "张先生"));
		pptUtil.replaceTagInParagraph(textParagraphs.get(4), newMaps("times", "2022年12月"));
	}

	private static void appendRow(PPTUtil pptUtil, XSLFTable table, int size, int margin, List<List<String>> values) {
		if (values != null) {
			values.forEach(x -> {
				XSLFTableRow row = table.addRow();
				x.forEach(y -> createCell(pptUtil, row.addCell(), size, y, margin));
			});
		}
	}

	private static void createBoldCell(PPTUtil pptUtil, XSLFTableCell cell, int size, String text, int margin) {
		createCell(pptUtil, cell, size, text, margin, true);
	}


	private static void createCell(PPTUtil pptUtil, XSLFTableCell cell, int size, String text, int margin) {
		createCell(pptUtil, cell, size, text, margin, false);
	}


	private static void createCell(PPTUtil pptUtil, XSLFTableCell cell, int size, String text, int margin,
			boolean bold) {
		XSLFTextParagraph textParagraph = pptUtil.addCellParagraph(cell);
		textParagraph.setFontAlign(TextParagraph.FontAlign.CENTER);
		pptUtil.addParagraphText(textParagraph, true, text);
		pptUtil.setCellFontfamily(cell, "微软雅黑");    // 字体
		pptUtil.setCellFontSize(cell, String.valueOf(size));  // 字体大小
		pptUtil.setCellHorizontalAlign(cell, "center"); // 单元格水平对齐方式
		pptUtil.setCellVerticalAlign(cell, "center");    // 单元格垂直对齐方式
		pptUtil.setCellColor(cell, "#000000");
		pptUtil.setCellFillColor(cell, "#ffffff");    // 单元格填充颜色
		if (bold) {
			pptUtil.setCellBold(cell, true);
		}
		if (margin > 0) {
			cell.setTopInset(margin);
			cell.setBottomInset(margin);
		}
		cell.setBorderWidth(TableCell.BorderEdge.top, 1);
		cell.setBorderWidth(TableCell.BorderEdge.right, 1);
		cell.setBorderWidth(TableCell.BorderEdge.left, 1);
		cell.setBorderWidth(TableCell.BorderEdge.bottom, 1);
		cell.setBorderColor(TableCell.BorderEdge.top, Color.BLACK);
		cell.setBorderColor(TableCell.BorderEdge.right, Color.BLACK);
		cell.setBorderColor(TableCell.BorderEdge.left, Color.BLACK);
		cell.setBorderColor(TableCell.BorderEdge.bottom, Color.BLACK);
	}


	private static Map<String, Object> newMaps(String key, String value) {
		Map<String, Object> paramMap = new HashMap<>(1);
		paramMap.put(key, value);
		return paramMap;
	}

	private static String fmt(double d) {
		return NumberFormat.getNumberInstance().format(d);
	}



			/*pptUtil.setCellColor(tableRow.getCells().get(0), "#ff0000");    // 字体颜色
		pptUtil.setCellFontfamily(tableRow.getCells().get(1), "宋体");    // 字体
		pptUtil.setCellFontWesternFontfamily(tableRow.getCells().get(1), "宋体"); // 西文字体
		pptUtil.setCellFontSize(tableRow.getCells().get(2), "18");  // 字体大小
		pptUtil.setCellFillColor(tableRow.getCells().get(3), "#ff0000");    // 单元格填充颜色
		pptUtil.setCellBold(tableRow.getCells().get(4), true);  // 单元格文本是否加粗
		pptUtil.setCellStrike(tableRow.getCells().get(5), true);    // 单元格文本是否加删除线
		pptUtil.setCellItalic(tableRow.getCells().get(6), true);    // 单元格文本是否斜体
		pptUtil.setCellUnderline(tableRow.getCells().get(7), true); // 单元格文本是否加下划线
		pptUtil.setCellHorizontalAlign(tableRow.getCells().get(8), "left"); // 单元格水平对齐方式
		pptUtil.setCellVerticalAlign(tableRow.getCells().get(9), "top");    // 单元格垂直对齐方式
		XSLFTableRow tableRow = pptUtil.getTableRows().get(2); // 获取第三行
		XSLFTextParagraph textParagraph = pptUtil.addCellParagraph(tableRow.getCells().get(4));
		pptUtil.addParagraphText(textParagraph, true, "9999");
		System.out.println(tableRow.getHeight());
		StrokeStyle styletop = tableRow.getCells().get(0).getBorderStyle(TableCell.BorderEdge.top);
		StrokeStyle stylebottom = tableRow.getCells().get(0).getBorderStyle(TableCell.BorderEdge.bottom);
		StrokeStyle styleleft = tableRow.getCells().get(0).getBorderStyle(TableCell.BorderEdge.left);
		StrokeStyle styleright = tableRow.getCells().get(0).getBorderStyle(TableCell.BorderEdge.right);

		*/
}