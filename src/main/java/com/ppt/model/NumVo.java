package com.ppt.model;

/**
 * @author Jack
 */
public class NumVo {

	private String category;//类别
	private String name;//名称
	private Double value;//金额

	public NumVo(){}

	public NumVo(String name, Double value) {
		this.name = name;
		this.value = value;
	}

	public NumVo(String category, String name, Double value) {
		this.name = name;
		this.value = value;
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}