package com.ppt.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ppt 数据
 *  <b>文本的换行用转义字符\n来表示</b>
 *  <b>ppt 模板结构不能随意更改,否则可能导致无法正常显示</>
 */
public class PptData {

	private String name = " ";//姓名
	private String time = " ";//日期
	private List<String> requirements = new ArrayList<>();//家庭财务需求
	private List<List<String>> familyMembers = new ArrayList<>();//家庭成员信息表 [姓名/出生日期/年龄/性别/退休或留学年份]
	private List<NumVo> propertys = new ArrayList<>();//资产负债表-资产 (需添加类别)
	private List<NumVo> debts = new ArrayList<>();//资产负债表-负债
	private List<NumVo> incomes = new ArrayList<>();//收入支出表-收入 (需添加类别)
	private List<NumVo> expands = new ArrayList<>();//收入支出表-支出
	private List<Double> financialAnalysis = new ArrayList<>();//财务分析和诊断(比率) [流动性比率/负债比率/清偿比率/结余比率/投资比率/负债收入比/被动收入比]
	private List<Double> assetLayout = new ArrayList<>();//资产配置布局 [流动性/防御性/收益性] (基准值皆为100)

	//////////////财务比率分析////////////////
	private String liquidityRatio = " "; //流动性比率分析
	private List<Double> liquidityRatioAmount = new ArrayList<>();//流动性比率分析对应金额 [流动性资金/家庭月支出]
	private String debtRatio = " ";//负债比率分析
	private List<Double> debtRatioAmount = new ArrayList<>();//负债比率分析对应金额 [净资产总额/负债总额]
	private String debtServiceRatio = " "; //清偿比率分析
	private String balanceRatio = " "; //结余比率分析
	private List<Double> balanceRatioAmount = new ArrayList<>();//结余比率分析对应金额 [年结余/年支出]
	private String investmentRatio = " ";//投资比率分析
	private List<Double> investmentRatioAmount = new ArrayList<>();//投资比率分析对应金额 [非投资资产/投资资产]
	private String debtToIncomeRatio = " ";//负债收入比率分析
	private List<Double> debtToIncomeRatioAmount = new ArrayList<>();//负债收入比率分析对应金额 [当年贷款支出/其他支出+结余]
	private String passiveIncomeRatio = " "; //被动收入比率分析
	private List<Double> passiveIncomeRatioAmount = new ArrayList<>();//被动收入比率分析对应金额 [被动收入覆盖支出/工作收入覆盖支出]
	private String summaryOfAnalysis = " ";//小结
	//////////////财务比率分析////////////////

	//////////////家庭风险保障分析////////////////
	//分析1(家庭风险保障现状)
	// [家庭成员/是否有社保/人寿险(保额)/人寿险(保费)/重疾险(保额)/重疾险(保费)/年金险(保额)/年金险(保费)/医疗险(保额)/医疗险(保费)/意外险(保额)/意外险(保费)/合计年交保费（元）]
	private List<List<String>> familyRiskSituations = new ArrayList<>();
	private String riskAnalysisTitle_2 = " ";//分析2的标题
	private String riskAnalysisContent_2 = " ";//分析2的正文
	private String riskAnalysisTitle_3 = " ";//分析3的标题
	private String riskAnalysisContent_3 = " ";//分析3的正文
	private String riskAnalysisTitle_4 = " ";//分析4的标题
	private String riskAnalysisContent_4 = " ";//分析4的正文
	private String riskAnalysisTitle_5 = " ";//分析5的标题
	private String riskAnalysisContent_5 = " ";//分析5的正文
	private String summaryOfRiskAnalysis = " ";//小结
	//////////////家庭风险保障分析////////////////

	//////////////资产结构及收支结构分析//////////////////
	private String structureAnalysisTitle_1 = " ";//分析1标题
	private String structureAnalysisContent_1 = " ";//分析1正文
	private String structureAnalysisTitle_2 = " ";//分析2标题
	private String structureAnalysisContent_2 = " ";//分析2正文
	private String summaryOfStructureAnalysis = " ";//小结
	//////////////资产结构及收支结构分析//////////////////

	//////////////配置建议//////////////////
	private String suggestionsTitle_1 = " ";//建议1标题
	private List<List<String>> familySeriousDiseaseInsuranceNeeds = new ArrayList<>();//现阶段家庭重疾险保障需求 [家庭成员/重疾险现有保额（万）/重疾险应配置保额（万)/重疾险建议补充额度（万）]
	private String suggestionsContent_1 = " ";//建议1正文
	private String suggestionsTitle_2 = " ";//建议2标题
	private String suggestionsContent_2 = " ";//建议2正文
	private String suggestionsTitle_3 = " ";//建议3标题
	private String suggestionsContent_3 = " ";//建议3正文
	private String suggestionsTitle_4 = " ";//建议4标题 (投资建议)
	private String suggestionsTableDesc_1 = " ";//建议4正文内容 (资产调整建议描述)
	private List<List<String>> assetAdjustmentSuggestions = new ArrayList<>();//资产调整建议 [序号/类别/原金额（万)/保留金额（万）/变动金额（万）/备注]
	private Double assetAdjustmentTotalAmount = 0.0;//资产调整建议总金额
	private String suggestionsTableDesc_2 = " ";//建议4正文内容(年结余规划描述)
	private List<List<String>> annualBalancePlanning = new ArrayList<>();//年结余规划 [家庭年结余（万）/用于家庭应急支出（万）/用于投资（万）]

	private String suggestionsTableDesc_3 = " ";//建议4正文内容(投资规划描述1)
	private List<List<String>> investmentPlanning1 = new ArrayList<>();//投资规划1 (最多60条) [投资年数/年份/年龄/投资金额/账户价值/子女教育/养老金/账户剩余价值]
	private String suggestionsTableDesc_4 = " ";//建议4正文内容(投资规划总结1)

	private String suggestionsTableDesc_5 = " ";//建议4正文内容(投资规划描述2)
	private List<List<String>> investmentPlanning2 = new ArrayList<>();//投资规划2 (最多60条) [投资年数/年份/年龄/投资金额/账户价值/子女教育/养老金/账户剩余价值]
	private String suggestionsTableDesc_6 = " ";//建议4正文内容(投资规划总结2)
	//////////////配置建议//////////////////

	private String allocationPlan = " "; //配置方案 (理财师自定义增加)

	private Map<String, List<Double>> assetAllocationChangesBeforeAndAfter = new HashMap<>(); //资产配置前后变化 key=收入类别 value=[配置前金额/配置后第5年金额/配置后第10年金额]


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<String> getRequirements() {
		return requirements;
	}

	public void setRequirements(List<String> requirements) {
		this.requirements = requirements;
	}

	public List<List<String>> getFamilyMembers() {
		return familyMembers;
	}

	public void setFamilyMembers(List<List<String>> familyMembers) {
		this.familyMembers = familyMembers;
	}

	public List<NumVo> getPropertys() {
		return propertys;
	}

	public void setPropertys(List<NumVo> propertys) {
		this.propertys = propertys;
	}

	public List<NumVo> getDebts() {
		return debts;
	}

	public void setDebts(List<NumVo> debts) {
		this.debts = debts;
	}

	public List<NumVo> getIncomes() {
		return incomes;
	}

	public void setIncomes(List<NumVo> incomes) {
		this.incomes = incomes;
	}

	public List<NumVo> getExpands() {
		return expands;
	}

	public void setExpands(List<NumVo> expands) {
		this.expands = expands;
	}

	public List<Double> getFinancialAnalysis() {
		return financialAnalysis;
	}

	public void setFinancialAnalysis(List<Double> financialAnalysis) {
		this.financialAnalysis = financialAnalysis;
	}

	public List<Double> getAssetLayout() {
		return assetLayout;
	}

	public void setAssetLayout(List<Double> assetLayout) {
		this.assetLayout = assetLayout;
	}

	public String getLiquidityRatio() {
		return liquidityRatio;
	}

	public void setLiquidityRatio(String liquidityRatio) {
		this.liquidityRatio = liquidityRatio;
	}

	public List<Double> getLiquidityRatioAmount() {
		return liquidityRatioAmount;
	}

	public void setLiquidityRatioAmount(List<Double> liquidityRatioAmount) {
		this.liquidityRatioAmount = liquidityRatioAmount;
	}

	public String getDebtRatio() {
		return debtRatio;
	}

	public void setDebtRatio(String debtRatio) {
		this.debtRatio = debtRatio;
	}

	public List<Double> getDebtRatioAmount() {
		return debtRatioAmount;
	}

	public void setDebtRatioAmount(List<Double> debtRatioAmount) {
		this.debtRatioAmount = debtRatioAmount;
	}

	public String getDebtServiceRatio() {
		return debtServiceRatio;
	}

	public void setDebtServiceRatio(String debtServiceRatio) {
		this.debtServiceRatio = debtServiceRatio;
	}

	public String getBalanceRatio() {
		return balanceRatio;
	}

	public void setBalanceRatio(String balanceRatio) {
		this.balanceRatio = balanceRatio;
	}

	public List<Double> getBalanceRatioAmount() {
		return balanceRatioAmount;
	}

	public void setBalanceRatioAmount(List<Double> balanceRatioAmount) {
		this.balanceRatioAmount = balanceRatioAmount;
	}

	public String getInvestmentRatio() {
		return investmentRatio;
	}

	public void setInvestmentRatio(String investmentRatio) {
		this.investmentRatio = investmentRatio;
	}

	public List<Double> getInvestmentRatioAmount() {
		return investmentRatioAmount;
	}

	public void setInvestmentRatioAmount(List<Double> investmentRatioAmount) {
		this.investmentRatioAmount = investmentRatioAmount;
	}

	public String getDebtToIncomeRatio() {
		return debtToIncomeRatio;
	}

	public void setDebtToIncomeRatio(String debtToIncomeRatio) {
		this.debtToIncomeRatio = debtToIncomeRatio;
	}

	public List<Double> getDebtToIncomeRatioAmount() {
		return debtToIncomeRatioAmount;
	}

	public void setDebtToIncomeRatioAmount(List<Double> debtToIncomeRatioAmount) {
		this.debtToIncomeRatioAmount = debtToIncomeRatioAmount;
	}

	public String getPassiveIncomeRatio() {
		return passiveIncomeRatio;
	}

	public void setPassiveIncomeRatio(String passiveIncomeRatio) {
		this.passiveIncomeRatio = passiveIncomeRatio;
	}

	public List<Double> getPassiveIncomeRatioAmount() {
		return passiveIncomeRatioAmount;
	}

	public void setPassiveIncomeRatioAmount(List<Double> passiveIncomeRatioAmount) {
		this.passiveIncomeRatioAmount = passiveIncomeRatioAmount;
	}

	public String getSummaryOfAnalysis() {
		return summaryOfAnalysis;
	}

	public void setSummaryOfAnalysis(String summaryOfAnalysis) {
		this.summaryOfAnalysis = summaryOfAnalysis;
	}

	public List<List<String>> getFamilyRiskSituations() {
		return familyRiskSituations;
	}

	public void setFamilyRiskSituations(List<List<String>> familyRiskSituations) {
		this.familyRiskSituations = familyRiskSituations;
	}

	public String getRiskAnalysisTitle_2() {
		return riskAnalysisTitle_2;
	}

	public void setRiskAnalysisTitle_2(String riskAnalysisTitle_2) {
		this.riskAnalysisTitle_2 = riskAnalysisTitle_2;
	}

	public String getRiskAnalysisContent_2() {
		return riskAnalysisContent_2;
	}

	public void setRiskAnalysisContent_2(String riskAnalysisContent_2) {
		this.riskAnalysisContent_2 = riskAnalysisContent_2;
	}

	public String getRiskAnalysisTitle_3() {
		return riskAnalysisTitle_3;
	}

	public void setRiskAnalysisTitle_3(String riskAnalysisTitle_3) {
		this.riskAnalysisTitle_3 = riskAnalysisTitle_3;
	}

	public String getRiskAnalysisContent_3() {
		return riskAnalysisContent_3;
	}

	public void setRiskAnalysisContent_3(String riskAnalysisContent_3) {
		this.riskAnalysisContent_3 = riskAnalysisContent_3;
	}

	public String getRiskAnalysisTitle_4() {
		return riskAnalysisTitle_4;
	}

	public void setRiskAnalysisTitle_4(String riskAnalysisTitle_4) {
		this.riskAnalysisTitle_4 = riskAnalysisTitle_4;
	}

	public String getRiskAnalysisContent_4() {
		return riskAnalysisContent_4;
	}

	public void setRiskAnalysisContent_4(String riskAnalysisContent_4) {
		this.riskAnalysisContent_4 = riskAnalysisContent_4;
	}

	public String getRiskAnalysisTitle_5() {
		return riskAnalysisTitle_5;
	}

	public void setRiskAnalysisTitle_5(String riskAnalysisTitle_5) {
		this.riskAnalysisTitle_5 = riskAnalysisTitle_5;
	}

	public String getRiskAnalysisContent_5() {
		return riskAnalysisContent_5;
	}

	public void setRiskAnalysisContent_5(String riskAnalysisContent_5) {
		this.riskAnalysisContent_5 = riskAnalysisContent_5;
	}

	public String getSummaryOfRiskAnalysis() {
		return summaryOfRiskAnalysis;
	}

	public void setSummaryOfRiskAnalysis(String summaryOfRiskAnalysis) {
		this.summaryOfRiskAnalysis = summaryOfRiskAnalysis;
	}

	public String getStructureAnalysisTitle_1() {
		return structureAnalysisTitle_1;
	}

	public void setStructureAnalysisTitle_1(String structureAnalysisTitle_1) {
		this.structureAnalysisTitle_1 = structureAnalysisTitle_1;
	}

	public String getStructureAnalysisContent_1() {
		return structureAnalysisContent_1;
	}

	public void setStructureAnalysisContent_1(String structureAnalysisContent_1) {
		this.structureAnalysisContent_1 = structureAnalysisContent_1;
	}

	public String getStructureAnalysisTitle_2() {
		return structureAnalysisTitle_2;
	}

	public void setStructureAnalysisTitle_2(String structureAnalysisTitle_2) {
		this.structureAnalysisTitle_2 = structureAnalysisTitle_2;
	}

	public String getStructureAnalysisContent_2() {
		return structureAnalysisContent_2;
	}

	public void setStructureAnalysisContent_2(String structureAnalysisContent_2) {
		this.structureAnalysisContent_2 = structureAnalysisContent_2;
	}

	public String getSummaryOfStructureAnalysis() {
		return summaryOfStructureAnalysis;
	}

	public void setSummaryOfStructureAnalysis(String summaryOfStructureAnalysis) {
		this.summaryOfStructureAnalysis = summaryOfStructureAnalysis;
	}

	public String getSuggestionsTitle_1() {
		return suggestionsTitle_1;
	}

	public void setSuggestionsTitle_1(String suggestionsTitle_1) {
		this.suggestionsTitle_1 = suggestionsTitle_1;
	}

	public List<List<String>> getFamilySeriousDiseaseInsuranceNeeds() {
		return familySeriousDiseaseInsuranceNeeds;
	}

	public void setFamilySeriousDiseaseInsuranceNeeds(List<List<String>> familySeriousDiseaseInsuranceNeeds) {
		this.familySeriousDiseaseInsuranceNeeds = familySeriousDiseaseInsuranceNeeds;
	}

	public String getSuggestionsContent_1() {
		return suggestionsContent_1;
	}

	public void setSuggestionsContent_1(String suggestionsContent_1) {
		this.suggestionsContent_1 = suggestionsContent_1;
	}

	public String getSuggestionsTitle_2() {
		return suggestionsTitle_2;
	}

	public void setSuggestionsTitle_2(String suggestionsTitle_2) {
		this.suggestionsTitle_2 = suggestionsTitle_2;
	}

	public String getSuggestionsContent_2() {
		return suggestionsContent_2;
	}

	public void setSuggestionsContent_2(String suggestionsContent_2) {
		this.suggestionsContent_2 = suggestionsContent_2;
	}

	public String getSuggestionsTitle_3() {
		return suggestionsTitle_3;
	}

	public void setSuggestionsTitle_3(String suggestionsTitle_3) {
		this.suggestionsTitle_3 = suggestionsTitle_3;
	}

	public String getSuggestionsContent_3() {
		return suggestionsContent_3;
	}

	public void setSuggestionsContent_3(String suggestionsContent_3) {
		this.suggestionsContent_3 = suggestionsContent_3;
	}

	public String getSuggestionsTitle_4() {
		return suggestionsTitle_4;
	}

	public void setSuggestionsTitle_4(String suggestionsTitle_4) {
		this.suggestionsTitle_4 = suggestionsTitle_4;
	}

	public String getSuggestionsTableDesc_1() {
		return suggestionsTableDesc_1;
	}

	public void setSuggestionsTableDesc_1(String suggestionsTableDesc_1) {
		this.suggestionsTableDesc_1 = suggestionsTableDesc_1;
	}

	public List<List<String>> getAssetAdjustmentSuggestions() {
		return assetAdjustmentSuggestions;
	}

	public void setAssetAdjustmentSuggestions(List<List<String>> assetAdjustmentSuggestions) {
		this.assetAdjustmentSuggestions = assetAdjustmentSuggestions;
	}

	public String getSuggestionsTableDesc_2() {
		return suggestionsTableDesc_2;
	}

	public void setSuggestionsTableDesc_2(String suggestionsTableDesc_2) {
		this.suggestionsTableDesc_2 = suggestionsTableDesc_2;
	}

	public List<List<String>> getAnnualBalancePlanning() {
		return annualBalancePlanning;
	}

	public void setAnnualBalancePlanning(List<List<String>> annualBalancePlanning) {
		this.annualBalancePlanning = annualBalancePlanning;
	}

	public String getSuggestionsTableDesc_3() {
		return suggestionsTableDesc_3;
	}

	public void setSuggestionsTableDesc_3(String suggestionsTableDesc_3) {
		this.suggestionsTableDesc_3 = suggestionsTableDesc_3;
	}

	public List<List<String>> getInvestmentPlanning1() {
		return investmentPlanning1;
	}

	public void setInvestmentPlanning1(List<List<String>> investmentPlanning1) {
		this.investmentPlanning1 = investmentPlanning1;
	}

	public String getSuggestionsTableDesc_4() {
		return suggestionsTableDesc_4;
	}

	public void setSuggestionsTableDesc_4(String suggestionsTableDesc_4) {
		this.suggestionsTableDesc_4 = suggestionsTableDesc_4;
	}

	public String getSuggestionsTableDesc_5() {
		return suggestionsTableDesc_5;
	}

	public void setSuggestionsTableDesc_5(String suggestionsTableDesc_5) {
		this.suggestionsTableDesc_5 = suggestionsTableDesc_5;
	}

	public List<List<String>> getInvestmentPlanning2() {
		return investmentPlanning2;
	}

	public void setInvestmentPlanning2(List<List<String>> investmentPlanning2) {
		this.investmentPlanning2 = investmentPlanning2;
	}

	public String getSuggestionsTableDesc_6() {
		return suggestionsTableDesc_6;
	}

	public void setSuggestionsTableDesc_6(String suggestionsTableDesc_6) {
		this.suggestionsTableDesc_6 = suggestionsTableDesc_6;
	}

	public String getAllocationPlan() {
		return allocationPlan;
	}

	public void setAllocationPlan(String allocationPlan) {
		this.allocationPlan = allocationPlan;
	}

	public Map<String, List<Double>> getAssetAllocationChangesBeforeAndAfter() {
		return assetAllocationChangesBeforeAndAfter;
	}

	public void setAssetAllocationChangesBeforeAndAfter(
			Map<String, List<Double>> assetAllocationChangesBeforeAndAfter) {
		this.assetAllocationChangesBeforeAndAfter = assetAllocationChangesBeforeAndAfter;
	}

	public Double getAssetAdjustmentTotalAmount() {
		return assetAdjustmentTotalAmount;
	}

	public void setAssetAdjustmentTotalAmount(Double assetAdjustmentTotalAmount) {
		this.assetAdjustmentTotalAmount = assetAdjustmentTotalAmount;
	}
}